//
//  CategoriesViewController.swift
//  Classify
//
//  Created by Nara on 04/09/2020.
//  Copyright © 2020 Yaisa. All rights reserved.
//

import UIKit

class CategoriesViewController: UIViewController {
    
    //core data context to interact and achieve persistence
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    @IBOutlet weak var categoriesTableView: UITableView!
    var categories = [Category]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        categoriesTableView.delegate = self
        categoriesTableView.dataSource = self
    
        retrieveCategories()
    }
    
    func retrieveCategories(){
        
        //logic to retrieve categories from core data
        do {
            self.categories.removeAll()
            self.categories = try context.fetch(Category.fetchRequest())
            
            DispatchQueue.main.async {
                self.categoriesTableView.reloadData()
            }
        }
        catch {
            print("An error was found while retrieving the categories")
        }
    }
    
    @IBAction func openCategoryDetails(_ sender: Any) {        
            let vc = getCategoryViewController()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func getCategoryViewController() -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        //retrieve the details/update view controller using the identifier
        let categoryVC = storyboard.instantiateViewController(withIdentifier: "Category") as! CategoryViewController
        
        //assign the update function that the child controller will call when a new category is saved
        categoryVC.updateFunc = {
            DispatchQueue.main.async {
                self.retrieveCategories()
            }
        }
        
        return categoryVC
    }
}

extension CategoriesViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let vc = getCategoryViewController() as! CategoryViewController
        vc.category = categories[indexPath.row]
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension CategoriesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let category = self.categories[indexPath.row]
        let cell = categoriesTableView.dequeueReusableCell(withIdentifier: "categoryCell", for: indexPath)
        cell.textLabel?.text = category.title
        return cell
    }
    
    
}
