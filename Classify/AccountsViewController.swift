//
//  SecondViewController.swift
//  Classify
//
//  Created by Nara on 30/08/2020.
//  Copyright © 2020 Yaisa. All rights reserved.
//

import UIKit

class AccountsViewController: UIViewController {

    //core data context to interact and achieve persistence
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    @IBOutlet weak var accountsTableView: UITableView!
    var accounts = [Account]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        accountsTableView.delegate = self
        accountsTableView.dataSource = self
        
        retrieveAccounts()
    }
    
    func retrieveAccounts(){
        
        //logic to retrieve accounts from core data
        do {
            self.accounts.removeAll()
            self.accounts = try context.fetch(Account.fetchRequest())
            
            DispatchQueue.main.async {
                self.accountsTableView.reloadData()
            }
        }
        catch {
            print("An error was found while retrieving the accounts")
        }
    }
    
    @IBAction func openAccountDetails(_ sender: Any) {
        let vc = getAccountViewController()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func getAccountViewController() -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        //retrieve the details/update view controller using the identifier
        let accountVC = storyboard.instantiateViewController(withIdentifier: "Account") as! AccountViewController
        
        //assign the update function that the child controller will call when a new account is saved
        accountVC.updateFunc = {
            DispatchQueue.main.async {
                self.retrieveAccounts()
            }
        }
        
        return accountVC
    }
}

extension AccountsViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let vc = getAccountViewController() as! AccountViewController
        vc.account = self.accounts[indexPath.row]
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension AccountsViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return accounts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let account = self.accounts[indexPath.row]
        let cell = accountsTableView.dequeueReusableCell(withIdentifier: "accountCell", for: indexPath)
        cell.textLabel?.text = account.title
        return cell
    }
    
    
}


