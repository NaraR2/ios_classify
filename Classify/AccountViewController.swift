//
//  AccountViewController.swift
//  Classify
//
//  Created by Nara on 05/09/2020.
//  Copyright © 2020 Yaisa. All rights reserved.
//

import UIKit

class AccountViewController: UIViewController, UITextFieldDelegate {

    //core data context to interact and achieve persistence
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var account: Account?
    @IBOutlet weak var accountTitle: UITextField!
    
    var updateFunc: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        accountTitle.delegate = self
        
        if (account != nil){
            accountTitle.text = account?.title
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        saveAccount(self)
        return true
    }
    
    @IBAction func saveAccount(_ sender: Any) {
        guard let accountTitle = accountTitle.text, !accountTitle.isEmpty else {
            return
        }
        
        //determine whether the account needs to be created or updated and set fields accordingly
        if (account != nil){
            //update
            account?.title = accountTitle
        }
        else {
            //create
            account = Account(context: self.context)
            account!.title = accountTitle
        }
        
        //save changes to core data context
        do {
            try self.context.save()
        }
        catch {
            print("An error was thrown while saving an account")
        }
        
        //call update function passed by the list view controller that will refetch the list items and refresh the tableview
        updateFunc?()
        
        //dismiss this view controller once the save is completed
        navigationController?.popViewController(animated: true)
    }
}
