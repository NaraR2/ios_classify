//
//  OperationsViewController.swift
//  Classify
//
//  Created by Nara on 06/09/2020.
//  Copyright © 2020 Yaisa. All rights reserved.
//

import UIKit

class OperationsViewController: UIViewController {

    //core data context to interact and achieve persistence
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var account: Account?
    @IBOutlet weak var accountTitle: UILabel!
    var operations = [Operation]()
    @IBOutlet weak var operationsTableView: UITableView!
    
     var updateFunc: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.operationsTableView.delegate = self
        self.operationsTableView.dataSource = self
        //self.operationsTableView.estimatedRowHeight = UITableView.automaticDimension
        self.accountTitle.text = account?.title
        retrieveOperations()
    }
    
    func retrieveOperations(){
        self.operations.removeAll()
        self.operations = self.account?.operations?.allObjects as! [Operation]
            
        DispatchQueue.main.async {
            self.operationsTableView.reloadData()
        }
    }
    
    @IBAction func openOperationDetails(_ sender: Any) {
        let vc = getOperationViewController() as! OperationViewController
        vc.account = self.account
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func getOperationViewController() -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let operationVC = storyboard.instantiateViewController(withIdentifier: "Operation") as! OperationViewController
        //assign the update function that the child controller will call when a new account is saved
        operationVC.updateFunc = {
            DispatchQueue.main.async {
                self.retrieveOperations()
            }
        }
        return operationVC
    }
    
    override func willMove(toParent parent: UIViewController?) {
        super.willMove(toParent: parent)
        if(parent == nil){
            updateFunc?()
        }
    }
}

extension OperationsViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let vc = getOperationViewController() as! OperationViewController
        vc.account = self.account
        vc.operation = self.operations[indexPath.row]
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension OperationsViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return operations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let operation = self.operations[indexPath.row]
        let cell = operationsTableView.dequeueReusableCell(withIdentifier: "operationCell", for: indexPath)
        cell.textLabel?.numberOfLines = 0
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        
        cell.textLabel?.text =
        """
        Amount: \(operation.amount),
        Concept: \(operation.concept ?? "-"),
        Category: \(operation.category?.title ?? "-"),
        Date: \(dateFormatter.string(from: operation.date!))
        """
        return cell
    }    
}

