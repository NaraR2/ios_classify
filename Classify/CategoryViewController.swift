//
//  CategoryViewController.swift
//  Classify
//
//  Created by Nara on 04/09/2020.
//  Copyright © 2020 Yaisa. All rights reserved.
//

import UIKit

class CategoryViewController: UIViewController, UITextFieldDelegate {

    //core data context to interact and achieve persistence
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var category: Category?
    @IBOutlet weak var categoryTitle: UITextField!
    
    var updateFunc: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.categoryTitle.delegate = self        
        if (category != nil){
            self.categoryTitle.text = category?.title
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        saveCategory(self)
        return true
    }

    @IBAction func saveCategory(_ sender: Any) {
        guard let categoryTitle = categoryTitle.text, !categoryTitle.isEmpty else {
            return
        }
        
        //determine whether category needs to created or updated and set fields accordingly
        if (category != nil){
            //update
            category?.title = categoryTitle
        }
        else {
            //create
            category = Category(context: self.context)
            category!.title = categoryTitle
        }
        
        //save changes to core data context
        do {
            try self.context.save()
        }
        catch {
            print("An error was thrown while saving a category")
        }
        
        //call update function passed by the categories list view controller that will refetch the categories and refresh the tableview
        updateFunc?()
        
        //dismiss this view controller once the save is completed
        navigationController?.popViewController(animated: true)
    }
}
