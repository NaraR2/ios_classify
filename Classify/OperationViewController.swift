//
//  OperationViewController.swift
//  Classify
//
//  Created by Nara on 06/09/2020.
//  Copyright © 2020 Yaisa. All rights reserved.
//

import UIKit

class OperationViewController: UIViewController {

    //core data context to interact and achieve persistence
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var account: Account?
    var operation: Operation?
    
    @IBOutlet weak var accountTitle: UILabel!
    @IBOutlet weak var operationAmount: UITextField!
    @IBOutlet weak var operationConcept: UITextField!
    var categories: [Category]?
    var category: Category?
    @IBOutlet weak var categoryPicker: UIPickerView!
    var categoryPickerData: [String] = [String]()
    @IBOutlet weak var operationDate: UIDatePicker!
    
    var updateFunc: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.categoryPicker.delegate = self
        self.categoryPicker.dataSource = self
        self.operationAmount.delegate = self
        
        self.accountTitle.text = account?.title
        
        //retrieve categories from core data
        retrieveCategoriesData()
        
        //if operation is available then set values for fields and controller variables
        if(operation != nil) {
            self.operationAmount.text = String(format: "%.1f", operation!.amount)
            self.operationConcept.text = operation?.concept
            self.category = operation?.category
            //mark default category selected
            if let indexCategory = categoryPickerData.firstIndex(of: self.category!.title!) {
                self.categoryPicker.selectRow(indexCategory, inComponent: 0, animated: true)
            }
            self.operationDate.date = operation!.date!
        }
    }
    
    func retrieveCategoriesData(){
        //logic to retrieve categories from core data
        do {
            self.categories = try context.fetch(Category.fetchRequest())
            self.categoryPickerData = [String]()
            for index in 0..<self.categories!.count {
                let category = self.categories![index] as Category
                self.categoryPickerData.append(category.title!)
            }
        }
        catch {
            print("An error was found while retrieving the categories")
        }
    }
    
    @IBAction func saveOperation(_ sender: Any) {
        let operationAmount = Double(self.operationAmount.text!)
        let operationConcept = self.operationConcept.text
        
        //determine whether the operation needs to be created or updated and set fields accordingly
        if (self.operation != nil){
            //update
            self.operation?.amount = operationAmount!
            self.operation?.concept = operationConcept
            self.operation?.category = category!
            self.operation?.date = self.operationDate.date
        }
        else {
            //create
            self.operation = Operation(context: self.context)
            self.operation?.account = self.account
            self.operation?.amount = operationAmount!
            self.operation?.concept = operationConcept
            self.operation?.category = category!
            self.operation?.date = self.operationDate.date
        }
        
        //save changes to core data context
        do {
            try self.context.save()
        }
        catch {
            print("An error was thrown while saving an operation")
        }
        
        //call update function passed by the operations list view controller that will refetch the operations and refresh the tableview
        updateFunc?()
        
        //dismiss this view controller once the save is completed
        navigationController?.popViewController(animated: true)
    }
    
}


extension OperationViewController : UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let selectedCategoryTitle = self.categoryPickerData[row]
        self.category = self.categories![row]
        return selectedCategoryTitle
    }
}

extension OperationViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.categoryPickerData.count
    }
}

extension OperationViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let inverseSet = NSCharacterSet(charactersIn:"0123456789").inverted
        let components = string.components(separatedBy: inverseSet)
        let filtered = components.joined(separator: "")
        
        if filtered == string {
            return true
        } else {
            if string == "." {
                let countdots = textField.text!.components(separatedBy:".").count - 1
                if countdots == 0 {
                    return true
                }else {
                    if countdots > 0 && string == "." {
                        return false
                    } else {
                        return true
                    }
                }
            }else {
                return false
            }
        }
    }
}

