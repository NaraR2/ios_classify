//
//  FirstViewController.swift
//  Classify
//
//  Created by Nara on 30/08/2020.
//  Copyright © 2020 Yaisa. All rights reserved.
//

import UIKit
import CoreData
import Charts

class HomeViewController: UIViewController {

    //core data context to interact and achieve persistence
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var accounts: [Account]?
    var account: Account?
    @IBOutlet weak var accountsPicker: UIPickerView!    
    var accountsPickerData: [String] = [String]()
    @IBOutlet weak var pieChart: PieChartView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.accountsPicker.delegate = self
        self.accountsPicker.dataSource = self
        retrieveAccountsData()
    }

    func retrieveAccountsData(){
        //logic to retrieve accounts from core data
        do {
            self.accounts = try context.fetch(Account.fetchRequest())
            self.accountsPickerData = [String]()
            for index in 0..<self.accounts!.count {
                let account = self.accounts![index]
                self.accountsPickerData.append(account.title!)
            }
        }
        catch {
            print("An error was found while retrieving the accounts")
        }
        
        DispatchQueue.main.async {
            self.account = self.accounts![0]
            self.setupPieChart()
        }
    }
    
    @IBAction func openAccountOperations(_ sender: Any) {
        let vc = getOperationsViewController() as! OperationsViewController
        vc.account = self.account
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func getOperationsViewController() -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let operationsVC = storyboard.instantiateViewController(withIdentifier: "Operations") as! OperationsViewController
        operationsVC.updateFunc = {
            DispatchQueue.main.async {
                self.setupPieChart()
            }
        }
        return operationsVC
    }
    
    func setupPieChart(){
        let operations = self.account?.operations?.allObjects as! [Operation]
        
        //calculate categories and amounts collection to pass it to the chart
        var categories: [String] = []
        var amounts: [Double] = []
        for operation in operations {
            let operationCategory = operation.category?.title
            if (!categories.contains(operationCategory!)) {
                categories.append(operationCategory!)
                amounts.append(operation.amount)
            } else {
                let i = categories.firstIndex(of: operationCategory!)!
                amounts[i] += operation.amount
            }
        }
        
        customizeChart(dataPoints: categories, values: amounts.map{ Double($0) })
    }
    
    /*
     In this method we create the pieChartData set from parameters
     and customize the pieCharView
     */
    func customizeChart(dataPoints: [String], values: [Double]) {
        // 1. Set ChartDataEntry
        var dataEntries: [ChartDataEntry] = []
        for i in 0..<dataPoints.count {
            let dataEntry = PieChartDataEntry(value: values[i], label: dataPoints[i], data: dataPoints[i] as AnyObject)
            dataEntries.append(dataEntry)
        }
        // 2. Set ChartDataSet
        let pieChartDataSet = PieChartDataSet(entries: dataEntries, label: nil)
        pieChartDataSet.colors = colorsOfCharts(numbersOfColor: dataPoints.count)
        // 3. Set ChartData
        let pieChartData = PieChartData(dataSet: pieChartDataSet)
        let format = NumberFormatter()
        format.numberStyle = .none
        let formatter = DefaultValueFormatter(formatter: format)
        pieChartData.setValueFormatter(formatter)
        // 4. Assign it to the chart’s data
        pieChartData.setValueTextColor(UIColor.black)
        pieChart.data = pieChartData
        pieChart.backgroundColor = UIColor.white
    }
    
    /*
     Generate a ramdon color set for the categories pieChart
     */
    private func colorsOfCharts(numbersOfColor: Int) -> [UIColor] {
        var colors: [UIColor] = []
        for _ in 0..<numbersOfColor {
            let red = Double(arc4random_uniform(256))
            let green = Double(arc4random_uniform(256))
            let blue = Double(arc4random_uniform(256))
            let color = UIColor(red: CGFloat(red/255), green: CGFloat(green/255), blue: CGFloat(blue/255), alpha: 1)
            colors.append(color)
        }
        return colors
    }
    
}


extension HomeViewController : UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let selectedAccountTitle = self.accountsPickerData[row]
        self.account = self.accounts![row]
        self.setupPieChart()
        return selectedAccountTitle
    }
}

extension HomeViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.accountsPickerData.count
    }
}

